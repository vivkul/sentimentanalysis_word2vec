Trainin Dataset: 25000 IMDb movie reviews labelled with 1 or 0
Test Dataset: 25000 IMDb movie reviews

Text Preprocessing:
-Removing HTML tags
-Removing non-letters present in review
-Converting whole text to lower-case
-removing stop words

Feature Extraction:
-Average of word vectors
-Bag of centroids
-Bag of words
-Term Frequency-inverse document frequency (TF-IDF)
-Word vector + TFIDF value

Feature Selection:
-SelectKbest: Removes all but the K highest scoring features
-SelectPercentile: Removes all but a user-specified highest scoring percentage of features

Classifiers:
-Random Forests
-Perceptron
-LDA
-SVM
-Logistic Regression





TODO:
word2vec documentation: https://code.google.com/p/word2vec/
1. Train word2vec (On training set VS On whole ImDB dataset VS On Wikipedia VS On google News). Analyse performance measure on differnt parametes at "Performance" on https://code.google.com/p/word2vec/
3. Use RNN by socher.
5. Find good attributes for paragraph using word2vec.
	1. We can give cluster mean some no to define the no of words in that cluster
	2. Use cluster means as features to train using RF and NN.
	3. We can restrict like first feature should have mean of cluster having maximum adjectives (Clustering on vectors), second on nouns etc. 
	4.* Get the vectors from a paragraph and put it in one of the two bags based on their class. 
	5. Apply R-NN as mentioned in http://www.socher.org/index.php/Main/SemanticCompositionalityThroughRecursiveMatrix-VectorSpaces
The methods dscribed in tutorial could not achieve very high accuracies because of the loss of word order during feature set formation. One solution suggested in the tutorial itself is to use Paragraph vectors instead of Word2vec. Go through this link for more information- http://cs.stanford.edu/~quocle/paragraph_vector.pdf

In all the methods described in the tutorial, they have used unsupervised learning for forming the feature sets. What if we do supervised learning, i.e., we need to assign a feature vector having known that a review is good(or bad), this might help in solving the problem of sarcasm.