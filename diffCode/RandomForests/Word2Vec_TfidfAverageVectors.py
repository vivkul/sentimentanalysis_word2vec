from gensim.models import Word2Vec
from sklearn.cluster import KMeans
import time
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from bs4 import BeautifulSoup
import re
from nltk.corpus import stopwords
import numpy as np
import os
from KaggleWord2VecUtility import KaggleWord2VecUtility
import pickle 
from sklearn.lda import LDA
from sklearn.qda import QDA
with open('trainDataVecs.csv', 'rb') as f:
    trainDataVecs= pickle.load(f)
with open('testDataVecs.csv', 'rb') as f:
    testDataVecs= pickle.load(f)

from sklearn.preprocessing import Imputer
imp=Imputer(missing_values='NaN',strategy='mean',axis=0)
trainDataVecs = imp.fit_transform(trainDataVecs)
testDataVecs = imp.fit_transform(testDataVecs)

train = pd.read_csv( '../../labeledTrainData.tsv', header=0, delimiter="\t", quoting=3 )

import math
from text.blob import TextBlob

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

import re
import nltk
import numpy as np
from bs4 import BeautifulSoup

def cleanReview(review):
	review_text = BeautifulSoup(review).get_text()
	review_text = re.sub("[^a-zA-Z]"," ", review_text)
	review = review_text.lower()
	return review

blobList = []
for review in train["review"]:
	review=cleanReview(review)
    temp=review.decode('string_escape')
    tempblob=TextBlob(temp)
    blobList.append(tempblob)

for review in unlabeled_train["review"]:
	review=cleanReview(review)
    temp=review.decode('string_escape')
    tempblob=TextBlob(temp)
    blobList.append(tempblob)

for review in test["review"]:
	review=cleanReview(review)
    temp=review.decode('string_escape')
    tempblob=TextBlob(temp)
    blobList.append(tempblob)

for i, blob in enumerate(blobList):
    print("Top words in document {}".format(i + 1))
    scores = {word: tfidf(word, blob, blobList) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    for word, score in sorted_words[:3]:
        print("Word: {}, TF-IDF: {}".format(word, round(score, 5)))

Blob blbVal = Blob.valueof(str)