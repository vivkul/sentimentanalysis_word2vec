from gensim.models import Word2Vec
from sklearn.cluster import KMeans
import time
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from bs4 import BeautifulSoup
import re
from nltk.corpus import stopwords
import numpy as np
import os
from KaggleWord2VecUtility import KaggleWord2VecUtility
import pickle 
from sklearn.lda import LDA
from sklearn.qda import QDA
with open('trainDataVecs.csv', 'rb') as f:
    trainDataVecs= pickle.load(f)
with open('testDataVecs.csv', 'rb') as f:
    testDataVecs= pickle.load(f)

from sklearn.preprocessing import Imputer
imp=Imputer(missing_values='NaN',strategy='mean',axis=0)
trainDataVecs = imp.fit_transform(trainDataVecs)
testDataVecs = imp.fit_transform(testDataVecs)

train = pd.read_csv( '../../labeledTrainData.tsv', header=0, delimiter="\t", quoting=3 )
model=LDA()
model = model.fit( trainDataVecs, (train["sentiment"]) )
result = model.predict( testDataVecs )
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( "./Word2Vec_AverageVectorsLDA.csv", index=False, quoting=3 )
print "Wrote Word2Vec_AverageVectorsLDA.csv"
model=QDA()
model = model.fit( trainDataVecs, (train["sentiment"]) )
result = model.predict( testDataVecs )
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( "./Word2Vec_AverageVectorsQDA.csv", index=False, quoting=3 )
print "Wrote Word2Vec_AverageVectorsQDA.csv"



