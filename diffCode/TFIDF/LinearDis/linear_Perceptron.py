import os
from KaggleWord2VecUtility import KaggleWord2VecUtility
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn import cross_validation
import pandas as pd
import numpy as np

train = pd.read_csv('../../../labeledTrainData.tsv', header=0, delimiter="\t", quoting=3);	
test = pd.read_csv('../../../testData.tsv', header=0, delimiter="\t", quoting=3 );
y = train["sentiment"]  
print "Cleaning and parsing movie reviews...\n"      
traindata = []
for i in xrange( 0, len(train["review"])):
    traindata.append(" ".join(KaggleWord2VecUtility.review_to_wordlist(train["review"][i], False)))

testdata = []
for i in xrange(0,len(test["review"])):
    testdata.append(" ".join(KaggleWord2VecUtility.review_to_wordlist(test["review"][i], False)))

print 'vectorizing... ', 
tfv = TfidfVectorizer(min_df=3,  max_features=None, 
        strip_accents='unicode', analyzer='word',token_pattern=r'\w{1,}',
        ngram_range=(1, 1), use_idf=1,smooth_idf=1,sublinear_tf=1,
        stop_words = 'english')
		# the word being considered should occur in atleast 3 documents and we will consider one word or a set of two sonsecutive words (ngram). Stop words aren't considered. Logarithmic term frequency is considered (1 + log(tf): sublinear_tf). 
X_all = traindata + testdata
lentrain = len(traindata)

print "fitting pipeline... ",
tfv.fit(X_all)
X_all = tfv.transform(X_all)

X = X_all[:lentrain]			# X contains tfidf of each word as element in 25000 documents x 309659 words' matrix
X_test = X_all[lentrain:]

percept = Perceptron(penalty=None, alpha=0.0001, fit_intercept=True, n_iter=2000, shuffle=False, verbose=0, eta0=1.0, n_jobs=1, random_state=0, class_weight=None, warm_start=False)
percept.fit(X,y)
result = percept.predict(X_test)

output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )

# Use pandas to write the comma-separated output file
output.to_csv( './Bag_of_Words_model.csv', index=False, quoting=3);
print "Wrote results to Bag_of_Words_model.csv"