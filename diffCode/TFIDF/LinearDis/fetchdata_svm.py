import os
from KaggleWord2VecUtility import KaggleWord2VecUtility
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import cross_validation
from sklearn import svm
import pandas as pd
import numpy as np
import pickle
with open('X.csv','rb') as f:
	X = pickle.load(f)
with open('X_test.csv','rb') as f:
	X_test = pickle.load(f)
with open('y.csv','rb') as f:
	y = pickle.load(f)
train = pd.read_csv('../../../labeledTrainData.tsv', header=0, delimiter="\t", quoting=3);	
test = pd.read_csv('../../../testData.tsv', header=0, delimiter="\t", quoting=3 );
model= svm.SVC(C=10,gamma=0.001)
model.fit(X,y)
result=model.predict(X_test)
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( './Bag_of_Words_model.csv', index=False, quoting=3);
print "Wrote results to Bag_of_Words_model.csv"

#Result c=1000 gamma=0.001 0.88344
#Result c=10 gamma=0.0001 0.76092
#Result c=10 gamma=0.01 0.87944