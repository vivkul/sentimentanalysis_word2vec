
import pickle

from KaggleWord2VecUtility import KaggleWord2VecUtility
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import cross_validation
import pandas as pd
import numpy as np
from sklearn.feature_selection import VarianceThreshold

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest

train = pd.read_csv('../../../labeledTrainData.tsv', header=0, delimiter="\t", quoting=3);	
test = pd.read_csv('../../../testData.tsv', header=0, delimiter="\t", quoting=3 );
y = train["sentiment"]  
print "Cleaning and parsing movie reviews...\n"     


with open('../X.csv','rb') as f:
	X = pickle.load(f)
with open('../X_test.csv','rb') as f:
	X_test = pickle.load(f)
with open('../y.csv','rb') as f:
	y = pickle.load(f)


print "fitting ................";



lentrain = len(train);



#//////////////Method 1///////////////////////////////
selector = SelectKBest(chi2, k=100000).fit(X, y)
X = selector.transform(X);
X_test= selector.transform(X_test);

#///////////Method2

selector = SelectPercentile(chi2, k=100000).fit(X,y);
X = selector.transform(X);
X_test= selector.transform(X_test);



# sel = VarianceThreshold(threshold=(.8 * (1 - .8)));


# X_all = sel.fit_transform(X+X_test);

# X = X_all[:lentrain]			# X contains tfidf of each word as element in 25000 documents x 309659 words' matrix
# X_test = X_all[lentrain:]





# model = LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
#                          C=10, fit_intercept=True, intercept_scaling=1.0, 
#                          class_weight=None, random_state=None)
# print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(model, X, y, cv=20, scoring='roc_auc'))

# print "Retrain on all training data, predicting test labels...\n"




# model.fit(X,y)
# result = model.predict_proba(X_test)[:,1]



# gnb = GaussianNB();

# model = gnb.fit(X, y);
# result = model.predict(X_train);



# output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )

# # Use pandas to write the comma-separated output file
# output.to_csv( './bayesian.csv', index=False, quoting=3);
# print "Wrote results to Bag_of_Words_model.csv"


forest = RandomForestClassifier(n_estimators = 100)

#     # Fit the forest to the training set, using the bag of words as
#     # features and the sentiment labels as the response variable
#     #
#     # This may take a few minutes to run
forest = forest.fit( X.toarray(), y );

# result=forest.predict(X_test.toarray());
