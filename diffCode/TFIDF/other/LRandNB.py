import os
from KaggleWord2VecUtility import KaggleWord2VecUtility
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import cross_validation
from sklearn import svm
import pandas as pd
import numpy as np
import pickle
from sklearn.naive_bayes import MultinomialNB
#from nolearn.dbn import DBN

with open('X.csv','rb') as f:
	X = pickle.load(f)
with open('X_test.csv','rb') as f:
	X_test = pickle.load(f)
with open('y.csv','rb') as f:
	y = pickle.load(f)
train = pd.read_csv('C:/Users/lksk/labeledTrainData.tsv', header=0, delimiter="\t", quoting=3);	
test = pd.read_csv('C:/Users/lksk/testData.tsv', header=0, delimiter="\t", quoting=3 );

# ------------------Naive Bayes classifier----------------------------------------
#clf = BernoulliNB(alpha=1.0, binarize=0.0, class_prior=None, fit_prior=True)
#clf=GaussianNB()
clf = MultinomialNB(alpha=1.0, class_prior=None, fit_prior=True)

clf.fit(X,y);
result = [];
# for i in range(0, X_test.shape[0]):
	# result[i] = clf.predict(X_test)[:,1];
 	# print i;
# 'end'

result = clf.predict(X_test);
#--------------------------------------------------------------------------------

#-----------------LOGISTIC REGRESSION CLASSIFIER--------------------------
model = LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
                         C=10, fit_intercept=True, intercept_scaling=1, 
                         class_weight=None, random_state=None)
print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(model, X, y, cv=20, scoring='roc_auc'))

print "Retrain on all training data, predicting test labels...\n"



model.fit(X,y)
result = model.predict_proba(X_test)[:,1]

# model= svm.SVC(C=10,gamma=0.001)
# model.fit(X,y)
# result=model.predict(X_test)
output = pd.DataFrame( data={"id":test["id"], "sentiment":result} )
output.to_csv( './Bag_of_Words.csv', index=False, quoting=3);
print "Wrote results to Bag_of_Words.csv"

#Result c=1000 gamma=0.001 0.88344
#Result c=10 gamma=0.0001 0.76092
#Result c=10 gamma=0.01 0.87944