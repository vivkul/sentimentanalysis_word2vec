RV_linear.py : SVM(100,.001) = 0.883,
			   SVM(1,0) = 0.712 
fetchdata_svm.py:
		SVM(c=1000 gamma=0.001) 	0.88344
	 	SVM(c=10 gamma=0.0001) 		0.76092
	 	SVM(c=10 gamma=0.01) 		0.87944
linear.py : 
		(default) 0.95228
		(c=100 )  0.95604
		(c=1000)  0.95504
		(c=120 )  0.95595
		(c=80  )  0.95615
		(c=90  )  0.95609
		(c=10  )  0.95722

linear_stopWords.py (removing stopwords using KaggleWord2VecUtility) : 0.95351
linear_PCA.py (PCA couldn't be done on sparse matrix hence randomized, but its depricated, so finally TruncatedSVD used but then it took a lot of memory and soon stoped) :
linear_Perceptron.py (with default parameters, n_iter = 5) : 0.83308
					 (n_iter = 500,penalty=None) : 			 0.84040
					 (n_iter = 2000,penalty=None,shuffle=False) : 0.84448
					 (n_iter = 500,penalty=None,shuffle=False) :  0.84448
					 (n_iter = 100,penalty='l2') : 0.80492
					 (n_iter = 500,penalty='l2',shuffle=False) : 0.80372

Random Forests:
	Bag of Words = 0.844
	Bag of Centroids(k = 3298) : 0.83924
	Word2VecAvg = 0.83348 

Bernoulli Naive bayes (on TFIDF) giving 0.85080

Multinomial naive bayes (on TFIDF) 0.85728

Logistic Regression (C=10) 0.95722 

Neural networks (tfidf on DBN) - 0.88304 [-1, 5, -1], learn_rates = 0.1,
	epochs = 5,
	verbose = 1

qda(on average_wordVectors) = 0.765401
perceptron(on average_wordVectors) = 0.84948
lda(on average_wordVectors) = 0.86704
Logistic Regression(on average_wordVectors) = 0.93014

SVM(c=1000 gamma=0.001) (on average_wordVectors) = 	0.86760
SVM(c=1000 gamma=0.001) (on bagOfWords) = 	0.877802
SVM(c=1000 gamma=0.001) (on bagOfCentroids) = 	0.86950

Averaging with tfidf takes 344 days :).
PCA, TruncatedSVD, LDA, QDA on tfidf give memory error or require dense matrix.

Possible Method:
1. Logistic regression on wordvectores summed with tfidf as weight.


1. Word2VecAvg < Bag of Centroids (compared on RF) (as a phrase meaning shouldn't be sum of its constituents, so can't be characteristic of the phrase but bag of centroids is more characteristic of the paragraph)
2. Bag of Centroids < Bag of Words (compared on RF) (Bag of Centroids is like dimensionality reduction of Bag of Words hence some information should be lost)
3. Bag of Words < TFIDF vector (compared on SVM) (as variation in frequent words can affect the sentence while they shouldn't e.g. ) 
4. Bag of Centroids < TFIDF (compared on SVM) (As Bag of Words is poorer than TFIDF)

When Run on average_wordVectors:
	-Logistic Regressions 	0.93014
	-SVM 					0.86760
	-lda 					0.86704
	-Perceptron 			0.84948
	-Random Forests 		0.83348
	-qda 					0.765401

When Run on TFIDF:
	-Logistic Regressions				0.95722
	-SVM 								0.88344
	-Neural networks(Just on 5 epochs) 	0.88304
	Multinomial naive bayes 			0.85728
	-Bernoulli Naive bayes 				0.85080
	-Perceptron 						0.84448
	

1. Find average over IDF using vectorizer.
2. Compare accuracy with area under ROC curve.